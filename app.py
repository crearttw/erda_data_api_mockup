#!/usr/bin/env python
# coding=utf-8

from datetime import timedelta
import json
from flask import Flask

class NonAsciiJSONEncoder(json.JSONEncoder):
    def __init__(self, **kwargs):
        super(NonAsciiJSONEncoder, self).__init__(**kwargs)
        self.ensure_ascii = False 
        self.indent = 2

class Config(object):
    DEBUG = True
#    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + db_path
    APP_NAME = 'mock-erda-data-api'
    API_BASE_PATH = '/erda/v1'
    SECRET_KEY = 'erda-api-secret-key-!523%&*ghcHDXCGhgcDhv()987GVUJ-(gDYT'
    JWT_EXPIRATION_DELTA = timedelta(days=30)
    JWT_AUTH_URL_RULE = '/erda/v1/auth'
    SECURITY_REGISTERABLE = True
    SECURITY_RECOVERABLE = True
    SECURITY_TRACKABLE = True
    SECURITY_PASSWORD_HASH = 'sha512_crypt'
    SECURITY_PASSWORD_SALT = 'add_salt'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

app = Flask(__name__)
app.config.from_object(Config)
app.json_encoder = NonAsciiJSONEncoder
# db = SQLAlchemy(app)
