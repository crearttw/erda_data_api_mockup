#!/usr/bin/env python3
# coding=utf-8
import json
from flask import Flask, jsonify, request, abort, Response, g, Blueprint
from app import app
import pandas as pd

base_path = app.config['API_BASE_PATH']
print(base_path)

df = None
workbook_name = 'BI0519'
sheet_name = 'sheet7'
blueprint = Blueprint('%s.%s' % (workbook_name, sheet_name), __name__)

path = '{0}/workbooks/{1}/sheets/{2}/data'.format(base_path, workbook_name, sheet_name)
print(path)
@blueprint.route(path, methods=['GET'])
def data():
    result_df = df[:]
    # print('values:', request.values)
    page = request.values.get('page', None)
    # page = json.dumps([1018, "恋暖初茶常州九洲新世界广场店"])
    # print('page:', page)
    try:
        page = json.loads(page)
        page_columns = ['门店编号', '门店名称']
        if not page:
            result_df = df
        else:
            m = True
            for i in range(len(page_columns)):
                # print(i, page_columns[i], page[i])
                m = m & (df[page_columns[i]] == page[i])
            result_df = df[m]
    except (TypeError, IndexError):
        pass

    filters = request.values.get('filters', None)
    # print('filters:', filters)
    try:
        filters = json.loads(filters)
        if 'mn:日期' in filters:
            result_df = result_df[result_df['日期'].dt.month.isin(filters['mn:日期'])]
        if 'yr:日期' in filters:
            result_df = result_df[result_df['日期'].dt.year.isin(filters['yr:日期'])]
    except TypeError:
        pass

    # <rows>([federated.1601ndn0pt73c11gsgewc1vc1jqe].[sum:营业额含税:qk] + [federated.1601ndn0pt73c11gsgewc1vc1jqe].[sum:营业额不含税:qk])</rows>
    # <cols>([federated.1601ndn0pt73c11gsgewc1vc1jqe].[mn:日期:ok] / [federated.1601ndn0pt73c11gsgewc1vc1jqe].[yr:日期:ok])</cols>
    idx = pd.DatetimeIndex(result_df['日期'])
    result_df = result_df.groupby([idx.month, idx.year]).agg({'营业额含税': 'sum', '营业额不含税': 'sum'})
    index = list(result_df.index)
    data = {}
    for k in result_df:
        data[k] = list(result_df[k])

    retv = dict(code = 0, message = 'OK', values = request.values, page = page, result = dict(index = index, data = data))
    return jsonify(retv)

path = '{0}/workbooks/{1}/sheets/{2}/page'.format(base_path, workbook_name, sheet_name)
print(path)
@blueprint.route(path, methods=['GET'])
def page():
    # <pages>
    #   <column>[federated.1601ndn0pt73c11gsgewc1vc1jqe].[none:门店编号 - Split 1:ok]</column>
    #   <column>[federated.1601ndn0pt73c11gsgewc1vc1jqe].[none:门店名称:nk]</column>
    # </pages>
    page_df = df[['门店编号', '门店名称']]
    page_df = page_df.drop_duplicates()
    result = list(list(v) for k, v in page_df.iterrows())
    # result = page_df.
    retv = dict(code = 0, message = 'OK', result = result)
    return jsonify(retv)

path = '{0}/workbooks/{1}/sheets/{2}/filters'.format(base_path, workbook_name, sheet_name)
print(path)
@blueprint.route(path, methods=['GET'])
def filters():
        #   <filter class='categorical' column='[federated.1601ndn0pt73c11gsgewc1vc1jqe].[mn:日期:ok]'>
        #     <groupfilter function='union' user:ui-domain='relevant' user:ui-enumeration='inclusive' user:ui-marker='enumerate'>
        #       <groupfilter function='member' level='[mn:日期:ok]' member='1' />
        #       <groupfilter function='member' level='[mn:日期:ok]' member='2' />
        #       <groupfilter function='member' level='[mn:日期:ok]' member='3' />
        #       <groupfilter function='member' level='[mn:日期:ok]' member='4' />
        #       <groupfilter function='member' level='[mn:日期:ok]' member='5' />
        #     </groupfilter>
        #   </filter>
        #   <filter class='categorical' column='[federated.1601ndn0pt73c11gsgewc1vc1jqe].[yr:日期:ok]'>
        #     <groupfilter function='union' user:ui-domain='database' user:ui-enumeration='inclusive' user:ui-marker='enumerate'>
        #       <groupfilter function='member' level='[yr:日期:ok]' member='2016' />
        #       <groupfilter function='member' level='[yr:日期:ok]' member='2017' />
        #     </groupfilter>
        #   </filter>
        #   <filter class='categorical' column='[federated.1601ndn0pt73c11gsgewc1vc1jqe].[yr:日期:ok]'>
        #     <groupfilter function='member' level='[yr:日期:ok]' member='2017' user:ui-domain='relevant' user:ui-enumeration='inclusive' user:ui-marker='enumerate' />
        #   </filter>
        #   <filter class='categorical' column='[federated.1601ndn0pt73c11gsgewc1vc1jqe].[yr:日期:ok]'>
        #     <groupfilter function='level-members' level='[yr:日期:ok]' user:ui-enumeration='all' user:ui-marker='enumerate' />
        #   </filter>
        #   <filter class='categorical' column='[federated.1601ndn0pt73c11gsgewc1vc1jqe].[yr:日期:ok]'>
        #     <groupfilter function='union' user:ui-domain='relevant' user:ui-enumeration='inclusive' user:ui-marker='enumerate'>
        #       <groupfilter function='member' level='[yr:日期:ok]' member='2016' />
        #       <groupfilter function='member' level='[yr:日期:ok]' member='2017' />
        #     </groupfilter>
        #   </filter>
    filters = {
        "mn:日期": df['日期'].dt.month.unique(),
        "yr:日期": df['日期'].dt.year.unique(),
    }
    retv = dict(code = 0, message = 'OK', result = filters)
    # print(retv)
    return jsonify(retv)
