#!/usr/bin/env python3
# coding=utf-8

import urllib

from flask import Flask
from app import app

from flask import Flask
from flask_cors import CORS, cross_origin
CORS(app)

from flask.json import JSONEncoder
import numpy
class MyEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        elif isinstance(obj, numpy.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)
app.json_encoder = MyEncoder

print('preparing datasets...')
import pandas as pd
product_df = pd.read_csv('data/product.csv')
product_df['门店编号'] = product_df['门店编号'].astype('int')
product_df['日期'] = pd.to_datetime(product_df['日期'], format='%Y-%m-%d')
# product_df['yr:日期'], product_df['mn:日期'] = product_df['日期'].dt.year, product_df['日期'].dt.month

import mock_sheet7_data
mock_sheet7_data.df = product_df
app.register_blueprint(mock_sheet7_data.blueprint)

import mock_sheet6_data
mock_sheet6_data.df = product_df
app.register_blueprint(mock_sheet6_data.blueprint)

import mock_sheet2_data
mock_sheet2_data.df = product_df
app.register_blueprint(mock_sheet2_data.blueprint)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
