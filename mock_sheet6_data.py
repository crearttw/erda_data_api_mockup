#!/usr/bin/env python3
# coding=utf-8
import json
from flask import Flask, jsonify, request, abort, Response, g, Blueprint
from app import app
import pandas as pd

base_path = app.config['API_BASE_PATH']
print(base_path)

df = None
workbook_name = 'BI0519'
sheet_name = 'sheet6'
blueprint = Blueprint('%s.%s' % (workbook_name, sheet_name), __name__)

path = '{0}/workbooks/{1}/sheets/{2}/data'.format(base_path, workbook_name, sheet_name)
print(path)
@blueprint.route(path, methods=['GET'])
def data():
    result_df = df[:]
    filters = request.values.get('filters', None)
    try:
        filters = json.loads(filters)
        if 'mn:日期' in filters:
            result_df = result_df[result_df['日期'].dt.month.isin(filters['mn:日期'])]
        if 'yr:日期' in filters:
            result_df = result_df[result_df['日期'].dt.month.isin(filters['yr:日期'])]
    except TypeError:
        pass

    # <rows>([federated.1601ndn0pt73c11gsgewc1vc1jqe].[none:门店编号 - Split 1:ok] / ([federated.1601ndn0pt73c11gsgewc1vc1jqe].[none:门店名称:nk] / [federated.1601ndn0pt73c11gsgewc1vc1jqe].[yr:日期:ok]))</rows>
    # <rows total='true'>([federated.1evcj2v17yhces183tha80pvq0hy].[none:门店编号:ok] / [federated.1evcj2v17yhces183tha80pvq0hy].[none:门店名称:nk])</rows>
    # <cols total='true'>[federated.1evcj2v17yhces183tha80pvq0hy].[none:日:ok]</cols>
    idx = pd.DatetimeIndex(result_df['日期'])

    def get_cell_value():
        df = result_df.groupby([result_df['门店编号'], result_df['门店名称'], idx.year, idx.month]).agg({'营业额不含税': 'sum'})
        index = list(df.index)
        total = {}
        for k in df:
            total[k] = list(df[k])
        return index, total
    cell_index, cell_value = get_cell_value()
    # print(cell_index, cell_value)

    # <cols total='true'>[federated.1601ndn0pt73c11gsgewc1vc1jqe].[mn:日期:ok]</cols>
    # <column-instance column='[营业额含税]' derivation='Sum' name='[sum:营业额含税:vtsum:qk]' pivot='key' type='quantitative' visual-totals='Sum' />
    # <column-instance column='[营业额含税]' derivation='Sum' name='[sum:营业额含税:vtavg:qk]' pivot='key' type='quantitative' visual-totals='Sum' />
    # <column-instance column='[营业额含税]' derivation='Sum' name='[sum:营业额含税:qk]' pivot='key' type='quantitative' />
    # <panes>
    #     <pane>
    #     <view>
    #         <breakdown value='auto' />
    #     </view>
    #     <mark class='Automatic' />
    #     <encodings>
    #         <text column='[federated.1601ndn0pt73c11gsgewc1vc1jqe].[sum:营业额含税:vtsum:qk]' />
    #     </encodings>
    #     <style>
    #         <style-rule element='mark'>
    #         <format attr='mark-labels-show' value='true' />
    #         <format attr='mark-labels-cull' value='true' />
    #         </style-rule>
    #     </style>
    #     </pane>
    # <panes>
    #   <pane>
    #     <view>
    #       <breakdown value='auto' />
    #     </view>
    #     <mark class='Automatic' />
    #     <encodings>
    #       <text column='[federated.1601ndn0pt73c11gsgewc1vc1jqe].[sum:营业额含税:qk]' />
    #     </encodings>
    #     <style>
    #       <style-rule element='mark'>
    #         <format attr='mark-labels-show' value='true' />
    #         <format attr='mark-labels-cull' value='true' />
    #       </style-rule>
    #     </style>
    #   </pane>
    # </panes>

    def get_col_total():
        df = result_df.groupby([idx.month]).agg({'营业额不含税': 'sum'})
        # index = list(df.index)
        index = list(df.index)
        total = {}
        for k in df:
            total[k] = list(df[k])
        return index, total
    col_index, col_total = get_col_total()

    def get_row_total():
        df = result_df.groupby([result_df['门店编号'], result_df['门店名称'], idx.year]).agg({'营业额不含税': 'sum'})
        index = list(df.index)
        total = {}
        for k in df:
            total[k] = list(df[k])
        return index, total
    row_index, row_total = get_row_total()

    def get_sub_total():
        df = result_df.groupby([result_df['门店编号'], result_df['门店名称'], idx.month]).agg({'营业额不含税': 'sum'})
        index = list(df.index)
        total = {}
        for k in df:
            total[k] = list(df[k])
        return index, total
    sub_index, sub_total = get_sub_total()

    retv = dict(code = 0,
        message = 'OK',
        result = dict(
            cell_value = dict(index = cell_index, data = cell_value),
            col_total = dict(index = col_index, data = col_total),
            row_total = dict(index = row_index, data = row_total),
            sub_total = dict(index = sub_index, data = sub_total),
            )
        )
    return jsonify(retv)

path = '{0}/workbooks/{1}/sheets/{2}/page'.format(base_path, workbook_name, sheet_name)
print(path)
@blueprint.route(path, methods=['GET'])
def page():
    # page_df = df[['门店编号', '门店名称']]
    # page_df = page_df.drop_duplicates()
    # result = list(list(v) for k, v in page_df.iterrows())
    # retv = dict(code = 0, message = 'OK', result = result)
    retv = dict(code = 0, message = 'OK')
    return jsonify(retv)

path = '{0}/workbooks/{1}/sheets/{2}/filters'.format(base_path, workbook_name, sheet_name)
print(path)
@blueprint.route(path, methods=['GET'])
def filters():
    filters = {
        "yr:日期": df['日期'].dt.year.unique(),
        "mn:日期": df['日期'].dt.month.unique(),
    }
    retv = dict(code = 0, message = 'OK', result = filters)
    print(retv)
    return jsonify(retv)
